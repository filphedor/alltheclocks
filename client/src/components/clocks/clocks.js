import './clocks.scss';

import React from "react";

import { createBrowserRouter, RouterProvider } from "react-router-dom";

import SegmentPhysics from '/components/segment-physics/segment-physics';
import DotPhysics from '/components/dot-physics/dot-physics';



let Clocks = function() {
    const router = createBrowserRouter([
        {
          path: "/segment-physics",
          element: <SegmentPhysics/>,
        },
        {
            path: "/dot-physics",
            element: <DotPhysics/>,
        }
      ]);


    return (
        <div className={'clocks'}>
            <div className={'clocks__content'}>
                <RouterProvider router={router} />
            </div>
        </div>
    );
};

export default Clocks;