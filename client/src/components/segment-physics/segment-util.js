import Vector from '/util/vector';

let multAll = function(vecs, mult) {
    return vecs.map((vec) => {
        return Vector.scalarMult(vec, mult);
    });
};

let addAll = function(vecs, diff) {
    return vecs.map((vec) => {
        return Vector.add(vec, diff);
    });
};

class SegmentUtil {
    static getCharacterLocations(charSizeMult) {
        let charLocations = [
            {
                'x': -3,
                'y': 0
            },
            {
                'x': -1,
                'y': 0
            },
            {
                'x': 1,
                'y': 0
            },
            {
                'x': 3,
                'y': 0
            }
        ];

        return multAll(charLocations, charSizeMult);
    }

    static getSegmentLocations(segLocationMult) {
        let segLocations = [
            {
                'x': 0,
                'y': -2,
                'rotated': true
            },
            {
                'x': 1,
                'y': -1,
                'rotated': false
            },
            {
                'x': 1,
                'y': 1,
                'rotated': false
            },
            {
                'x': 0,
                'y': 2,
                'rotated': true
            },
            {
                'x': -1,
                'y': 1,
                'rotated': false
            },
            {
                'x': -1,
                'y': -1,
                'rotated': false
            },
            {
                'x': 0,
                'y': 0,
                'rotated': true
            }
        ];

        return multAll(segLocations, segLocationMult);
    }

    static getForcePoints(segmentLocation, index, segmentSizeMult) {
        let rotated = [
            true,
            false,
            false,
            true,
            false,
            false,
            true
        ];

        let points = [
            {
                'x': 0,
                'y': 3
            },
            {
                'x': 0,
                'y': -3
            }
        ];

        if (rotated[index]) {
            points = [
                {
                    'x': 3,
                    'y': 0
                },
                {
                    'x': -3,
                    'y': 0
                }
            ];
        }

        points = multAll(points, segmentSizeMult);
        points = addAll(points, segmentLocation);

        return points;
    }

    static getTimeCharacters() {
        let time = new Date();

        let zeroPad = function(num) {
            if (num < 10) {
                return '0' + num;
            }

            return String(num);
        };

        return zeroPad(time.getHours()) + zeroPad(time.getMinutes());
    }

    static getLight(char, i) {
        let charLightMap = {
            '0': [
                true,
                true,
                true,
                true,
                true,
                true,
                false
            ],
            '1': [
                false,
                true,
                true,
                false,
                false,
                false,
                false
            ],
            '2': [
                true,
                true,
                false,
                true,
                true,
                false,
                true
            ],
            '3': [
                true,
                true,
                true,
                true,
                false,
                false,
                true
            ],
            '4': [
                false,
                true,
                true,
                false,
                false,
                true,
                true
            ],
            '5': [
                true,
                false,
                true,
                true,
                false,
                true,
                true
            ],
            '6': [
                true,
                false,
                true,
                true,
                true,
                true,
                true
            ],
            '7': [
                true,
                true,
                true,
                false,
                false,
                false,
                false
            ],
            '8': [
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ],
            '9': [
                true,
                true,
                true,
                true,
                false,
                true,
                true
            ]
        };

        return charLightMap[char][i];
    }

    static getTimeTargets(charSizeMult, segmentLocationMult, segmentSizeMult) {
        let charLocations = SegmentUtil.getCharacterLocations(charSizeMult);
        let segmentLocationsByChar = [];

        charLocations.forEach((charLoc) => {
            let charSegmentLocations = this.getSegmentLocations(segmentLocationMult);

            charSegmentLocations = addAll(charSegmentLocations, charLoc);

            segmentLocationsByChar.push(charSegmentLocations);
        });

        let targetPointsByChar = [];

        segmentLocationsByChar.forEach((segmentLocations) => {
            let targetPoints = [];

            segmentLocations.forEach((segmentLoc, i) => {
                targetPoints.push(SegmentUtil.getForcePoints(segmentLoc, i, segmentSizeMult));
            });

            targetPointsByChar.push(targetPoints);
        });

        let timeChars = SegmentUtil.getTimeCharacters();

        let targets = [];

        targetPointsByChar.forEach((targetPoints, i) => {
            let char = timeChars[i];

            let newTargets = targetPoints.map((targetPoint, i) => {
                let light = SegmentUtil.getLight(char, i);

                return {
                    'points': targetPoint,
                    'isLit': light
                };
            });

            targets = targets.concat(newTargets);
        });

        return targets;
    }
}

export default SegmentUtil;
