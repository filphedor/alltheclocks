import { Body, Engine } from 'matter-js';

import Vector from '/util/vector';


class SegmentRunner {
    constructor(engine, delta, speed, segments) {
        this._running = false;
        this._engine = engine;
        this._delta = delta;
        this._speed = speed;
        this._segments = segments;
    }

    start() {
        this._running = true;

        this.step();
    }

    step() {
        let start = new Date();

        this._segments.forEach((segment) => {
            segment.update();
        });

        Engine.update(this._engine, this._delta);

        let end = new Date();

        let elapsed = end - start;

        let diff = this._delta - elapsed;

        if (this._running) {
            if (diff < 0) {
                console.log('lagging');
            }

            setTimeout(this.step.bind(this), Math.max(diff / this._speed, 1));
        }
    }

    stop() {
        this._running = false;
    }
}


export default SegmentRunner;
