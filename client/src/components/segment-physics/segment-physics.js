import './segment-physics.scss';

import React, { useEffect } from 'react';

import Runner from './segment-runner';

import Matter, { Engine, Body, Bodies, Composite } from 'matter-js';
import Vector from '/util/vector';

import Segment from './segment';

import SegmentDisplay from './segment-display';
import SegmentUtil from './segment-util';


function SegmentPhysics() {
    let setUp = function() {
        let charSizeMult = 250;
        let segmentLocationMult = 120;
        let segmentSizeMult = 20;

        var engine = Engine.create();

        var ground = Bodies.rectangle(0, 500, 4000, 50, { 'isStatic': true });

        let numSegments = 28;
        let segments = [];

        for (let i = 0; i < numSegments; i++) {
            segments.push(new Segment({
                'x': Math.random() * 1000 - 500,
                'y': Math.random() * 1000 - 500
            }, segmentSizeMult));
        }

        let targetPoints = SegmentUtil.getTimeTargets(charSizeMult, segmentLocationMult, segmentSizeMult);

        for (let i = 0; i < segments.length; i++) {
            segments[i].setTargetPoints(targetPoints[i]);
        }

        let segmentBodies = segments.map((segment) => {
            return segment.getBody();
        });

        Composite.add(engine.world, [...segmentBodies, ground]);

        var runner = new Runner(engine, 5, 1, segments);

        let magnet = true;

        let magnetOn = function() {
            magnet = true;

            segments.forEach((segment) => {
                segment.setMagnet(magnet);
            });

            setTimeout(magnetOff, 20000);
        };

        let magnetOff = function() {
            magnet = false;

            segments.forEach((segment) => {
                segment.setMagnet(magnet);
            });

            setTimeout(magnetOn, 10000);
        };

        magnetOn();

        setInterval(function() {
            let newTargets = SegmentUtil.getTimeTargets(charSizeMult, segmentLocationMult, segmentSizeMult);

            for (let i = 0; i < segments.length; i++) {
                segments[i].setTargetPoints(newTargets[i]);
            }
        }, 5000);

        runner.start();

        let segmentDisplay = new SegmentDisplay(segments);
        segmentDisplay.start();
    };

    useEffect(() => {
        setUp();
    }, []);

    return (
        <div/>
    );
}

export default SegmentPhysics;
