import * as THREE from 'three';

import { EffectComposer } from 'three/addons/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/addons/postprocessing/RenderPass.js';
import { UnrealBloomPass } from 'three/addons/postprocessing/UnrealBloomPass.js';


class SegmentDisplay {
    constructor(segments) {
        this._scene = new THREE.Scene();
        this._segments = segments;
    }

    start() {
        this._scene.background = new THREE.Color(0x111111);
        this._scene.fog = new THREE.Fog(this._scene.background, 1, 3000);
        const camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 10000);

        camera.quaternion.setFromAxisAngle(new THREE.Vector3(1, 0, 0), Math.PI / -32);

        camera.position.x = 0;
        camera.position.y = 0;
        camera.position.z = 1000;

        const groundShape = new THREE.Shape();

        let width = 4000;
        let depth = 50;
        groundShape.moveTo(-1 * width, -1 * width);
        groundShape.lineTo(width, -1 * width);
        groundShape.lineTo(width, width);
        groundShape.lineTo(-1 * width, width);
        groundShape.lineTo(-1 * width, -1 * width);

        const extrudeSettings = {
            'depth': depth,
            'bevelEnabled': true,
            'bevelSegments': 2,
            'steps': 2,
            'bevelSize': 0.1,
            'bevelThickness': 1
        };

        const groundGeometry = new THREE.ExtrudeGeometry(groundShape, extrudeSettings);

        const groundMaterial = new THREE.MeshLambertMaterial({ 'color': 0x111111 });

        let groundMesh = new THREE.Mesh(groundGeometry, groundMaterial);

        groundMesh.position.setX(0);
        groundMesh.position.setY(-475);
        groundMesh.quaternion.setFromAxisAngle(new THREE.Vector3(1, 0, 0), Math.PI / 2);
        groundMesh.receiveShadow = true;

        this._scene.add(groundMesh);

        const hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.6);
        hemiLight.color.setHSL(0.6, 1, 0.6);
        hemiLight.groundColor.setHSL(0.095, 1, 0.75);
        hemiLight.position.set(0, 2000, 0);
        this._scene.add(hemiLight);

        const dirLight = new THREE.DirectionalLight(0xffffff, 1);
        dirLight.target = groundMesh;
        dirLight.color.setHSL(0.1, 1, 0.95);
        dirLight.position.set(0, 1, 0);
        dirLight.position.multiplyScalar(1000);
        this._scene.add(dirLight);

        dirLight.castShadow = true;

        dirLight.shadow.mapSize.width = 2048;
        dirLight.shadow.mapSize.height = 2048;

        const d = 5000;

        dirLight.shadow.camera.left = - d;
        dirLight.shadow.camera.right = d;
        dirLight.shadow.camera.top = d;
        dirLight.shadow.camera.bottom = - d;

        dirLight.shadow.camera.far = 5000;
        dirLight.shadow.bias = - 0.0001;

        const renderer = new THREE.WebGLRenderer({
            'antialias': true
        });
        renderer.shadowMap.enabled = true;
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);

        const composer = new EffectComposer(renderer);

        const renderPass = new RenderPass(this._scene, camera);
        composer.addPass(renderPass);

        const bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
        bloomPass.threshold = 0.001;
        bloomPass.strength = 0.5;
        bloomPass.radius = 0;

        composer.addPass(bloomPass);

        this._segments.forEach((segment) => {
            this._scene.add(segment.getMesh());
        });

        let self = this;
        let animate = function() {
            requestAnimationFrame(animate);

            self._segments.forEach((segment) => {
                segment.updateDisplay();
            });

            composer.render();
        };

        animate();
    }
}

export default SegmentDisplay;
