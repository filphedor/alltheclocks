import Matter, { Engine, Render, Body, Bodies, Composite } from 'matter-js';
import * as THREE from 'three';

import Vector from '/util/vector';


let segmentBodyPoints = [
    {
        'x': -2,
        'y': 3
    },
    {
        'x': 0,
        'y': 4
    },
    {
        'x': 2,
        'y': 3
    },
    {
        'x': 2,
        'y': -3
    },
    {
        'x': 0,
        'y': -4
    },
    {
        'x': -2,
        'y': -3
    }
];

class Segment {
    constructor(location, sizeMult) {
        this._sizeMult = sizeMult;
        this._targetPoints = [];
        this._isMagnetOn = false;
        this._flickerCount = 0;
        this._flickerTime = 0;
        this._lastLit = false;
        this._isLit = false;

        this.initializeBody(location);
        this.initializeMesh();
    }

    getBody() {
        return this._body;
    }

    initializeBody(location) {
        let bodyPoints = segmentBodyPoints.map((vec) => {
            return Vector.scalarMult(vec, this._sizeMult);
        });

        this._body = Bodies.fromVertices(location.x, location.y, bodyPoints);
    }

    getMesh() {
        return this._mesh;
    }

    initializeMesh() {
        let bodyPoints = segmentBodyPoints.map((vec) => {
            return Vector.scalarMult(vec, this._sizeMult);
        });

        const seg = new THREE.Shape();

        seg.moveTo(bodyPoints[0].x, bodyPoints[0].y);

        for (let i = 1; i < bodyPoints.length; i++) {
            seg.lineTo(bodyPoints[i].x, bodyPoints[i].y);
        }

        seg.lineTo(bodyPoints[0].x, bodyPoints[0].y);


        const extrudeSettings = {
            'depth': 10,
            'bevelEnabled': true,
            'bevelSegments': 2,
            'steps': 2,
            'bevelSize': 0.1,
            'bevelThickness': 1
        };

        const geometry = new THREE.ExtrudeGeometry(seg, extrudeSettings);

        const material = new THREE.MeshLambertMaterial({ 'color': 0x00ff00 });

        let mesh = new THREE.Mesh(geometry, material);
        mesh.position.setX(this.getBody().position.x);
        mesh.position.setY(this.getBody().position.y);
        mesh.castShadow = true;
        mesh.receiveShadow = true;

        this._mesh = mesh;
    }

    updateDisplay() {
        this.getMesh().position.setX(this.getBody().position.x);
        this.getMesh().position.setY(this.getBody().position.y * -1);
        this.getMesh().quaternion.setFromAxisAngle(new THREE.Vector3(0, 0, 1), this.getBody().angle);

        if (this._isLit) {
            this.getMesh().material = new THREE.MeshLambertMaterial({ 'color': 0x00ff00 });
        } else {
            this.getMesh().material = new THREE.MeshLambertMaterial({ 'color': 0x111111 });
        }
    }

    setTargetPoints(points) {
        if (this._isLit !== points.isLit) {
            this.flicker();
        }

        this._targetPoints = points;
    }

    flicker() {
        //always odd so it switches
        let flickerCount = Math.floor(Math.random() * 9);

        if (flickerCount % 2 === 0) {
            flickerCount = flickerCount - 1;
        }

        this._flickerCount = flickerCount;
        this._flickerTime = 0;
    }

    setMagnet(magnet) {
        this._isMagnetOn = magnet;
    }

    update() {
        let forceMult = Math.pow(this._sizeMult, 2) * 100;
        let minForce = 0.0000006 * forceMult;
        let maxForce = 0.000002 * forceMult;

        let targetPoints = this._targetPoints.points;
        let bodyForcePoints = this.getForcePoints();

        let diffA = Vector.subtract(targetPoints[0], bodyForcePoints[0]);
        let diffB = Vector.subtract(targetPoints[1], bodyForcePoints[1]);

        let calcMagForce = function(r) {
            return forceMult / (4 * Math.PI * Math.pow(r, 2));
        };

        let sizeA = calcMagForce(Vector.length(diffA));
        let sizeB = calcMagForce(Vector.length(diffB));

        let useMinMax = true;

        if (useMinMax) {
            sizeA = Math.max(minForce, Math.min(sizeA, maxForce));
            sizeB = Math.max(minForce, Math.min(sizeB, maxForce));
        }

        let forceA = Vector.scalarMult(Vector.identity(diffA), sizeA);
        let forceB = Vector.scalarMult(Vector.identity(diffB), sizeB);

        if (this._isMagnetOn) {
            Body.applyForce(this.getBody(), bodyForcePoints[0], forceA);
            Body.applyForce(this.getBody(), bodyForcePoints[1], forceB);
        }

        if (this._flickerCount) {
            if (this._flickerTime) {
                this._flickerTime = this._flickerTime - 1;
            } else {
                this._flickerCount = this._flickerCount - 1;
                this._isLit = !this._isLit;
                this._flickerTime = Math.floor(Math.random() * 30);
            }
        }
    }

    getForcePoints() {
        let points = [
            {
                'x': 0,
                'y': 3
            },
            {
                'x': 0,
                'y': -3
            }
        ];

        let rotLoc = this._body.angle;

        points = points.map((vec) => {
            return Vector.scalarMult(vec, this._sizeMult);
        });

        points = points.map((vec) => {
            let origin = {
                'x': 0,
                'y': 0
            };

            return Vector.rotate(vec, origin, rotLoc);
        });

        points = points.map((vec) => {
            return Vector.add(vec, this._body.position);
        });

        return points;
    }
}

export default Segment;
