import './dot-physics.scss';

import React from 'react';
import * as THREE from 'three';


function DotPhysics() {

    let camera, scene, renderer;
    const mixers = [];
    let stats;

    const clock = new THREE.Clock();

    init();
    animate();

    function init() {
        camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 5000);
        camera.position.set(0, 0, 250);

        scene = new THREE.Scene();
        scene.background = new THREE.Color().setHSL(0, 0, 0);

        const dirLight = new THREE.DirectionalLight(0xffffff, 1);
        dirLight.color.setHSL(0.1, 1, 0.95);
        dirLight.position.set(- 1, 1.75, 1);
        dirLight.position.multiplyScalar(30);
        scene.add(dirLight);

        dirLight.castShadow = true;

        dirLight.shadow.mapSize.width = 2048;
        dirLight.shadow.mapSize.height = 2048;

        const d = 50;

        dirLight.shadow.camera.left = - d;
        dirLight.shadow.camera.right = d;
        dirLight.shadow.camera.top = d;
        dirLight.shadow.camera.bottom = - d;

        dirLight.shadow.camera.far = 3500;
        dirLight.shadow.bias = - 0.0001;

        const dirLightHelper = new THREE.DirectionalLightHelper(dirLight, 10);
        scene.add(dirLightHelper);

        // GROUND

        const groundGeo = new THREE.PlaneGeometry(10000, 10000);
        const groundMat = new THREE.MeshLambertMaterial({ 'color': 0xffffff });
        groundMat.color.setHSL(0.095, 1, 0.75);

        const ground = new THREE.Mesh(groundGeo, groundMat);
        ground.position.y = - 33;
        ground.rotation.x = - Math.PI / 2;
        ground.receiveShadow = true;
        scene.add(ground);

        const groundShape = new THREE.Shape();

        let width = 10;
        let depth = 50;
        groundShape.moveTo(-1 * width, -1 * width);
        groundShape.lineTo(width, -1 * width);
        groundShape.lineTo(width, width);
        groundShape.lineTo(-1 * width, width);
        groundShape.lineTo(-1 * width, -1 * width);

        const extrudeSettings = {
            'depth': depth,
            'bevelEnabled': true,
            'bevelSegments': 2,
            'steps': 2,
            'bevelSize': 0.1,
            'bevelThickness': 1
        };

        const groundGeometry = new THREE.ExtrudeGeometry(groundShape, extrudeSettings);

        const groundMaterial = new THREE.MeshBasicMaterial({ 'color': 0x222222 });

        let groundMesh = new THREE.Mesh(groundGeometry, groundMaterial);

        const s = 0.35;
        groundMesh.scale.set(s, s, s);
        groundMesh.position.y = 15;
        groundMesh.rotation.y = - 1;

        groundMesh.castShadow = true;
        groundMesh.receiveShadow = true;

        scene.add(groundMesh);


        // RENDERER

        renderer = new THREE.WebGLRenderer({ 'antialias': true });
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);
        renderer.outputEncoding = THREE.sRGBEncoding;
        renderer.shadowMap.enabled = true;

        window.addEventListener('resize', onWindowResize);

    }

    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    //

    function animate() {
        requestAnimationFrame(animate);

        render();
    }

    function render() {

        const delta = clock.getDelta();

        for (let i = 0; i < mixers.length; i ++) {

            mixers[i].update(delta);

        }

        renderer.render(scene, camera);

    }
    return (
        <div>
            {'DotPhysics'}
        </div>
    );
}

export default DotPhysics;
