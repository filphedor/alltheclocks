import './bundle.scss';

import React from 'react';
import { createRoot } from 'react-dom/client';

import Clocks from '/components/clocks/clocks';

const root = createRoot(document.getElementById('root'));
root.render(<Clocks/>);
