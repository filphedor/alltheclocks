class Vector {
    static scalarMult(vectorA, mult) {
        return {
            'x': vectorA.x * mult,
            'y': vectorA.y * mult
        };
    }

    static scalarDiv(vectorA, div) {
        return {
            'x': vectorA.x / div,
            'y': vectorA.y / div
        };
    }

    static add(vectorA, vectorB) {
        return {
            'x': vectorA.x + vectorB.x,
            'y': vectorA.y + vectorB.y
        };
    }

    static subtract(vectorA, vectorB) {
        return {
            'x': vectorA.x - vectorB.x,
            'y': vectorA.y - vectorB.y
        };
    }

    static rotate(vectorA, vectorB, angle) {
        let temp = Vector.subtract(vectorA, vectorB);

        let rot = {
            'x': (Math.cos(angle) * temp.x) - (Math.sin(angle) * temp.y),
            'y': (Math.cos(angle) * temp.y) + (Math.sin(angle) * temp.x)
        };

        return Vector.add(rot, vectorB);
    }

    static length(vectorA) {
        return Math.sqrt(Math.pow(vectorA.x, 2) + Math.pow(vectorA.y, 2));
    }

    static identity(vectorA) {
        let length = Vector.length(vectorA);

        return Vector.scalarDiv(vectorA, length);
    }
}

export default Vector;
